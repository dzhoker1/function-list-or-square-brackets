import random
from dis import dis
from timeit import timeit

import matplotlib.pyplot as plt

# step 1
data_one = []
data_two = list()

dis('data_one = []')
dis('data_two = list()')

data_one = [1, 2, 3]
data_two = list((1, 2, 3))

dis('data_one = [1, 2, 3]')
dis('data_two = list((1, 2, 3))')

y = (2, 4, 8, 12, 16)
x1 = [timeit('data_one = [1, 2]'),
      timeit('data_one = [1, 2, 3, 4]'),
      timeit('data_one = [1, 2, 3, 4, 5, 6, 7, 8]'),
      timeit('data_one = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]'),
      timeit('data_one = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16]')]


x2 = [timeit('data_two = list((1, 2))'),
      timeit('data_two = list((1, 2, 3, 4))'),
      timeit('data_two = list((1, 2, 3, 4, 5, 6, 7, 8))'),
      timeit('data_two = list((1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12))'),
      timeit('data_two = list((1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16))')]


plt.plot(y, x1, y, x2)
plt.legend(('[]', 'list()'))
plt.show()

# step 2
N = 10
data_one = [i for i in range(N)]
data_two = list(range(N))
data_three = [*range(N)]
print(data_one == data_two == data_three)
dis('data_one = [i for i in range(N)]')
print('*')
dis('data_two = list(range(N))')
print('*')
dis('data_three = [*range(N)]')

NUMBER = 10000
n = 50
result = [[], [], []]
y = []
while n < 1001:
    y.append(n)
    result[0].append(timeit('data_one = [i for i in range(n)]', number=NUMBER, setup=f'n={n}'))
    result[1].append(timeit('data_two = list(range(n))', number=NUMBER, setup=f'n={n}'))
    result[2].append(timeit('data_three = [*range(n)]', number=NUMBER, setup=f'n={n}'))
    n += 50

plt.plot(y, result[0], y, result[1], y, result[2])
plt.legend(('list comp', 'list()', '[]'))
plt.show()

# step 2.2
NUMBER = 100_000
n = 100
result = [[], []]
y = []
while n < 5001:
    y.append(n)
    result[0].append(timeit('data_two = list(range(n))', number=NUMBER, setup=f'n={n}'))
    result[1].append(timeit('data_three = [*range(n)]', number=NUMBER, setup=f'n={n}'))
    n += 100

plt.plot(y, result[0], y, result[1])
plt.legend(('list()', '[]'))
plt.show()

# step 3
N = 10
MIN = -100_000
MAX = 10_000_000
test_data_one = (random.randint(MIN, MAX) for _ in range(N))
test_data_two = (random.randint(MIN, MAX) for _ in range(N))
data_one = [*test_data_one]
data_two = list(test_data_two)
print(data_one)
print(data_two)
dis('data_one = [*test_data_one]')
print('*')
dis('data_two = list(test_data_two)')

NUMBER = 1_000_000
n = 100
result = [[], []]
y = []
while n < 10001:
    y.append(n)
    result[0].append(timeit('data_one = [*test_data]', number=NUMBER,
                            setup=f'test_data=(random.randint({MIN}, {MAX}) for _ in range({n}))', globals=globals()))
    result[1].append(timeit('data_two = list(test_data)', number=NUMBER,
                            setup=f'test_data=(random.randint({MIN}, {MAX}) for _ in range({n}))', globals=globals()))
    n += 100

plt.plot(y, result[0], y, result[1])
plt.legend(('[]', 'list()'))
plt.show()
